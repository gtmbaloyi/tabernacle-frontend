<?php
    include ('../Services/DBConnect.php');

    class dao extends GalleryDAO {
        private $conn;

        public function _construct(){
            $dbconn = new parent();
            $this->conn = $dbconn -> connect();
        }

        function select($table ='', $where ='', $other ) {
            if ($where != '') {
                $where = 'where '.$where;
            }

            $sql = "select * from ".$table ." ".$where. " ".$other;
            $sel = mysqli_query($this->conn, $sql) or die(mysqli_error($this->conn));

            return $sel;
        }

    }
?>