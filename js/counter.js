/*Start-of-evnt*/ 
var deadline = 'Aug 05 2018 10:00:00 UTC+2';
function time_remaining(endtime){
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor( (t/1000) % 60 );
  var minutes = Math.floor( (t/1000/60) % 60 );
  var hours = Math.floor( (t/(1000*60*60)) % 24 );
  var days = Math.floor( t/(1000*60*60*24) );
  return {'total':t, 'days':days, 'hours':hours, 'minutes':minutes, 'seconds':seconds};
}
function run_clock(id,endtime){
  var clock = document.getElementById(id);
  
  // get spans where our clock numbers are held
  var days_span = clock.querySelector('.days');
  var hours_span = clock.querySelector('.hours');
  var minutes_span = clock.querySelector('.minutes');
  var seconds_span = clock.querySelector('.seconds');

  function update_clock(){
    var t = time_remaining(endtime);
    
    // update the numbers in each part of the clock
    days_span.innerHTML = t.days;
    hours_span.innerHTML = ('0' + t.hours).slice(-2);
    minutes_span.innerHTML = ('0' + t.minutes).slice(-2);
    seconds_span.innerHTML = ('0' + t.seconds).slice(-2);
    
    if(t.total<=0){ clearInterval(timeinterval); }
  }
  update_clock();
  var timeinterval = setInterval(update_clock,1000);
}
run_clock('sundayService',deadline);
/*End-of-event*/


/*Start-of-evnt*/ 
var deadline = 'Aug 7 2018 18:00:00 UTC+2';
function time_remaining(endtime){
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor( (t/1000) % 60 );
  var minutes = Math.floor( (t/1000/60) % 60 );
  var hours = Math.floor( (t/(1000*60*60)) % 24 );
  var days = Math.floor( t/(1000*60*60*24) );
  return {'total':t, 'days':days, 'hours':hours, 'minutes':minutes, 'seconds':seconds};
}
function run_clock(id,endtime){
  var clock = document.getElementById(id);
  
  // get spans where our clock numbers are held
  var days_span = clock.querySelector('.days');
  var hours_span = clock.querySelector('.hours');
  var minutes_span = clock.querySelector('.minutes');
  var seconds_span = clock.querySelector('.seconds');

  function update_clock(){
    var t = time_remaining(endtime);
    
    // update the numbers in each part of the clock
    days_span.innerHTML = t.days;
    hours_span.innerHTML = ('0' + t.hours).slice(-2);
    minutes_span.innerHTML = ('0' + t.minutes).slice(-2);
    seconds_span.innerHTML = ('0' + t.seconds).slice(-2);
    
    if(t.total<=0){ clearInterval(timeinterval); }
  }
  update_clock();
  var timeinterval = setInterval(update_clock,1000);
}
run_clock('TuesdayService',deadline);
/*End-of-event*/


/*Start-of-evnt*/ 
var deadline = 'Aug 01 2018 18:00:00 UTC+2';
function time_remaining(endtime){
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor( (t/1000) % 60 );
  var minutes = Math.floor( (t/1000/60) % 60 );
  var hours = Math.floor( (t/(1000*60*60)) % 24 );
  var days = Math.floor( t/(1000*60*60*24) );
  return {'total':t, 'days':days, 'hours':hours, 'minutes':minutes, 'seconds':seconds};
}
function run_clock(id,endtime){
  var clock = document.getElementById(id);
  
  // get spans where our clock numbers are held
  var days_span = clock.querySelector('.days');
  var hours_span = clock.querySelector('.hours');
  var minutes_span = clock.querySelector('.minutes');
  var seconds_span = clock.querySelector('.seconds');

  function update_clock(){
    var t = time_remaining(endtime);
    
    // update the numbers in each part of the clock
    days_span.innerHTML = t.days;
    hours_span.innerHTML = ('0' + t.hours).slice(-2);
    minutes_span.innerHTML = ('0' + t.minutes).slice(-2);
    seconds_span.innerHTML = ('0' + t.seconds).slice(-2);
    
    if(t.total<=0){ clearInterval(timeinterval); }
  }
  update_clock();
  var timeinterval = setInterval(update_clock,1000);
}
run_clock('WednessdayService',deadline);
/*End-of-event*/


/*Start-of-evnt*/ 
var deadline = 'Aug 03 2018 18:00:00 UTC+2';
function time_remaining(endtime){
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor( (t/1000) % 60 );
  var minutes = Math.floor( (t/1000/60) % 60 );
  var hours = Math.floor( (t/(1000*60*60)) % 24 );
  var days = Math.floor( t/(1000*60*60*24) );
  return {'total':t, 'days':days, 'hours':hours, 'minutes':minutes, 'seconds':seconds};
}
  function run_clock(id,endtime){
  var clock = document.getElementById(id);
  
  // get spans where our clock numbers are held
  var days_span = clock.querySelector('.days');
  var hours_span = clock.querySelector('.hours');
  var minutes_span = clock.querySelector('.minutes');
  var seconds_span = clock.querySelector('.seconds');

function update_clock(){
    var t = time_remaining(endtime);
    
    // update the numbers in each part of the clock
    days_span.innerHTML = t.days;
    hours_span.innerHTML = ('0' + t.hours).slice(-2);
    minutes_span.innerHTML = ('0' + t.minutes).slice(-2);
    seconds_span.innerHTML = ('0' + t.seconds).slice(-2);
    
    if(t.total<=0){ clearInterval(timeinterval); }
  }
  update_clock();
  var timeinterval = setInterval(update_clock,1000);
}
run_clock('FridayService',deadline);
/*End-of-event*/



(function() {
  var updateButton = document.getElementById('updateDetails');
  var cancelButton = document.getElementById('cancel');
  var favDialog = document.getElementById('favDialog');

  // Update button opens a modal dialog
  updateButton.addEventListener('click', function() {
    favDialog.showModal();
  });

  // Form cancel button closes the dialog box
  cancelButton.addEventListener('click', function() {
    favDialog.close();
  });
})();