(function() {
  var cancelButton = document.getElementById('cancel');
  var favDialog = document.getElementById('BankingDetails');

  // Update button opens a modal dialog
  ShowDetails.addEventListener('click', function() {
    favDialog.showModal();
  });

  // Form cancel button closes the dialog box
  cancelButton.addEventListener('click', function() {
    favDialog.close();
  });
})();